terraform {
  required_version = ">= 0.12"
  required_providers {
    aws = ">=3.38.0"
  }
  backend "s3" {
    region  = "eu-central-1"
    profile = "default"
    key     = "terraform-state-file/statefile.tfstate"
    bucket  = "terraform-state-bucket-cname-23"
  }
}

//need to create module
