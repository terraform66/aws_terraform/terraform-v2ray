variable "profile" {
  type        = string
  default     = "default"
  description = "The common profile for aws cli"
}

variable "instance_type" {
  type        = string
  default     = "t2.micro"
  description = "The common type of the instance instead t2-micro"
}

variable "region-common" {
  type        = string
  default     = "eu-central-1"
  description = "The common region in eu-central-1 for v2ray server"
}

variable "vpc_name" {
  description = "The name of the common VPC for the subnet and v2ray instance"
  type        = string
  default     = "The common VPC"
}

variable "vpc_cidr_block" {
  description = "The IPv4 CIDR block of the common VPC"
  type        = string
  default     = "10.0.1.0/16"
}

